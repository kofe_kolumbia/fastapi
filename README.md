# Необходимые модули для запуска 

Для запуска программного средства необходимо иметь docker версии не ниже 24.0.6

# Инструкция по запуску
перед запуском необходимо дать права файлу app_entrypoint.sh

для этого можно прописать команду:
```
sudo chmod +x ./fastapi/app_entrypoint.sh
```

Для запуска docker без прав администратора https://docs.docker.com/engine/install/linux-postinstall/


Создать и запустить PostgreSQL вместе с FastAPI:
```
docker compose up --build
```

* Откройте главную страницу по адресу http://localhost:8000/

* Откройте документацию по api по адресу http://localhost:8000/docs


Чтобы очистить базу данных:
```
docker compose down -v
```

чтобы очистить все:
```
docker system prune --all --volumes 
```