import os
import httpx
import logging
import app.db_models


from app.auth import *
from app.news import *
from pathlib import Path
from app.methods import *
from app.prepods import *
from typing import Annotated
from datetime import timedelta
from starlette.staticfiles import StaticFiles
from starlette.status import HTTP_302_FOUND
from fastapi.responses import RedirectResponse
from app.shemas import RegisterUser, ChangeUser, RegisterPrepod
from fastapi import FastAPI, Response, status, Depends, File, UploadFile, HTTPException, Request, Header, Body, Form
from typing import Optional
from starlette.responses import FileResponse, HTMLResponse, StreamingResponse
from app.db_connect import engine, SessionLocal
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates



UPLOADED_IMG_PATH_PREPODS = "static/img/prepods"
UPLOADED_FILES_PATH = Path() / 'uploads'
UPLOADED_IMG_PATH = "static/img/news"


server = FastAPI()
os.chdir("app")


app.db_models.Base.metadata.create_all(engine)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename="../main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))


templates = Jinja2Templates(directory="templates")
server.mount("/app/static", StaticFiles(directory="static"), name="static")


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@server.get('/')
def home(request: Request):
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    news = get_posts()
    important_news = get_post_by_genre('Важное')
    other_news = get_post_by_genre('Обычное')
    events = get_post_by_genre('Мероприятия')
    data_news = get_post_by_genre('Дата')
    return templates.TemplateResponse("index.html", {"request": request, 
                                                     "news": news,
                                                     "important_news": important_news,
                                                     "other_news": other_news,
                                                     "events": events,
                                                     "data_news": data_news})


@server.get('/important_news')
async def important_news(request: Request):
    important_news = get_post_by_genre('Важное')
    return templates.TemplateResponse("index.html", {"request": request, "news": important_news})


@server.get('/event_news')
async def important_news(request: Request):
    events = get_post_by_genre('Мероприятие')
    return templates.TemplateResponse("index.html", {"request": request, "news": events})


@server.get('/data_news')
async def important_news(request: Request):
    data_news = get_post_by_genre('Дата')
    return templates.TemplateResponse("index.html", {"request": request, "news": data_news})


@server.get('/other_news')
async def important_news(request: Request):
    other_news = get_post_by_genre('Обычное')
    return templates.TemplateResponse("index.html", {"request": request, "news": other_news})


@server.get('/admin')
def admin(request: Request):
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/admin\" status=\"HTTP_200_OK\"')
    news = get_posts()
    peoples = get_prepods()
    return templates.TemplateResponse("admin.html", {"request": request, "news": news, "peoples": peoples})


@server.get('/link-register')
def link_register(request: Request):
    return templates.TemplateResponse("registration.html", {"request": request})


@server.post("/register-user", tags=["Auth"])
def sex(data: str = Body(...)):
    url = server.url_path_for('home')
    a = data.split('&')
    username = a[0][9:]
    password = a[1][9:]
    user = get_user(username)
    if user:
        raise HTTPException(status_code=400, detail="Username already registered")
    create_user(username, password, "user")
    user = get_user(username)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.post("/admin/change-user", tags=["Admin"])
def change_user(change_user: ChangeUser, token: str = Depends(oauth2_scheme)):
    user_dict = change_user.dict()
    id = user_dict["id"]
    username = user_dict["username"]
    role = user_dict["role"]
    current_user = get_current_user(token)

    if current_user.role.name == "admin":
        return update_user(id, username, role)
    else:
        return "Permission denied"


@server.post("/admin/reset-password", tags=["Admin"])
def set_new_password(id: str, password: str, token: str = Depends(oauth2_scheme)):
    current_user = get_current_user(token)
    if current_user.role.name == "admin":
        return reset_password(id, password)
    else:
        return "Permission denied"


@server.get("/admin/get-users", tags=["Admin"])
def get_users_list(token: str = Depends(oauth2_scheme)):
    current_user = get_current_user(token)
    if current_user.role.name == "admin":
        return get_users()
    else:
        return "Permission denied"


@server.delete("/delete-user/{id}", tags=["Admin"])
def delete_news(id: int, token: str = Depends(oauth2_scheme)):
    current_user = get_current_user(token)
    if current_user.role.name == "admin":
        user = get_user(id=id)
        delete_user(user)
        return f"user {user.username} deleted"


@server.get("/user", tags=["User"])
async def get_user_info(token: str = Header(...)):
    user = get_current_user(token)
    if not user.avatar:
        user.avatar = "static/img/user_default.html"
    return user


@server.post("/token", tags=["Auth"], response_model=Token)
async def login_for_access_token(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    ser = get_current_user(access_token)
    return templates.TemplateResponse("tmp.html", {"request": request,
                                                   "name": user.username,
                                                   "role": user.role,
                                                   "avatar": user.avatar,
                                                   "gitId": user.git_id,
                                                   "access_token": access_token,
                                                   })


@server.get("/protected", tags=["Test"])
def protected_route(token: str = Depends(oauth2_scheme)):
    user = get_current_user(token)
    return {f"Hello, {user.role.name}"}
    # return user.username


# #############GITLAB#############

authorizationUrl = "https://gitlab.com/oauth/authorize"
tokenUrl = "https://gitlab.com/oauth/token"
scope = "read_user"

client_id = "c4d8e78028451515aad2d7180a06f20f66d006399bb1db6406f9173084236ffa"
client_secret = "6b623d758fedae3ffe913520e0c0ced8063123ec191c1c62723009d70c1303b7"
redirect_uri = "http://localhost:8000/auth/callback"


@server.get("/auth", tags=["Auth"])
async def url_for_gitlab():
    authorization_url = authorizationUrl + f"?response_type=code&client_id={client_id}&redirect_uri={redirect_uri}&scope={scope}"
    # localStorage.setItem('name', 'Alex');
    return RedirectResponse(f"{authorization_url}")
    # return {"url": authorization_url}


@server.get("/auth/callback", tags=["Auth"])
async def gitlab_auth_callback(request: Request, code: str):
    data = {
        "grant_type": "authorization_code",
        "client_id": client_id,
        "client_secret": client_secret,
        "code": code,
        "redirect_uri": redirect_uri,
    }

    async with httpx.AsyncClient() as client:
        response = await client.post(tokenUrl, data=data)

    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Failed to get access token")

    token = response.json()["access_token"]

    # return {"access_token": token}
    # token = "0b5b30ac972a7feafaf1b981dc844809c09c2bcddff26215d50436883e33e0d3"

    headers = {"Authorization": f"Bearer {token}"}
    async with httpx.AsyncClient() as client:
        response = await client.get("https://gitlab.com/api/v4/user", headers=headers)

    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Failed to get user profile")

    user_profile = response.json()

    if authenticate_git_user(user_profile):
        # вход через git
        user = authenticate_git_user(user_profile)
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": user.username}, expires_delta=access_token_expires
        )
        return templates.TemplateResponse("tmp.html", {"request": request,
                                                   "name": user.username,
                                                   "role": user.role,
                                                   "avatar": user.avatar,
                                                   "gitId": user.git_id,
                                                   "access_token": access_token,
                                                   })
    else:
        return templates.TemplateResponse("link.html", {"request": request, "user": user_profile})
    return user_profile


@server.get("/link/{token}/{git_id}/{git_username}/{git_name}")
def link(token: str, git_id: str, git_username: str, git_name: str):
    git_info = {'git_id': git_id, "git_username": git_username, "git_name": git_name}
    user = get_current_user(token)
    git_update_user(user.id, git_info)
    return RedirectResponse("/")


@server.get("/test/git", tags=["Test"])
def test_data_for_bind_git_and_account(token: str = Depends(oauth2_scheme)):
    data = {"id":"1771056","username":"14NAGIBATOR88","name":"Ilya","state":"active","avatar_url":"https://secure.gravatar.com/avatar/51f11cd7498e7df3c89e9a900ebd9978?s=80&d=identicon","web_url":"https://gitlab.com/14NAGIBATOR88","created_at":"2017-11-11T13:59:00.893Z","bio":"","location":"","public_email":"","skype":"","linkedin":"","twitter":"","discord":"","website_url":"","organization":"","job_title":"","pronouns":"null","bot":"false","work_information":"null","local_time":"7:52 PM","last_sign_in_at":"2023-08-20T20:59:03.072Z","confirmed_at":"2017-11-11T13:59:34.932Z","last_activity_on":"2023-09-10","email":"ilya.shlyahetko@gmail.com","theme_id":1,"color_scheme_id":2,"projects_limit":100000,"current_sign_in_at":"2023-09-10T19:17:26.115Z","identities":[],"can_create_group":"true","can_create_project":"true","two_factor_enabled":"false","external":"false","private_profile":"false","commit_email":"ilya.shlyahetko@gmail.com","shared_runners_minutes_limit":"null","extra_shared_runners_minutes_limit":"null","scim_identities":[]}
    user_id = (get_current_user(token)).id
    resp = git_update_user(id=user_id, data=data)
    #json_data = jsonable_encoder(data)
    #return JSONResponse(content=json_data)
    return resp


@server.post("/test/auth/git", tags=["Test"])
def login_via_gitlab(data=None):
    if data is None:
        data = {"id": "1771056", "username": "14NAGIBATOR88", "name": "Ilya", "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/51f11cd7498e7df3c89e9a900ebd9978?s=80&d=identicon",
                "web_url": "https://gitlab.com/14NAGIBATOR88", "created_at": "2017-11-11T13:59:00.893Z", "bio": "",
                "location": "", "public_email": "", "skype": "", "linkedin": "", "twitter": "", "discord": "",
                "website_url": "", "organization": "", "job_title": "", "pronouns": "null", "bot": "false",
                "work_information": "null", "local_time": "7:52 PM", "last_sign_in_at": "2023-08-20T20:59:03.072Z",
                "confirmed_at": "2017-11-11T13:59:34.932Z", "last_activity_on": "2023-09-10",
                "email": "ilya.shlyahetko@gmail.com", "theme_id": 1, "color_scheme_id": 2, "projects_limit": 100000,
                "current_sign_in_at": "2023-09-10T19:17:26.115Z", "identities": [], "can_create_group": "true",
                "can_create_project": "true", "two_factor_enabled": "false", "external": "false",
                "private_profile": "false", "commit_email": "ilya.shlyahetko@gmail.com",
                "shared_runners_minutes_limit": "null", "extra_shared_runners_minutes_limit": "null",
                "scim_identities": []}
    user = authenticate_git_user(data)
    #return user
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@server.get("/test/register", tags=["Test"])
def register_admin():
    user = create_user("a", "a", "admin")
    user = get_user("a")
    # return {"user": user}
    return {"username": user.username, "role": user.role}
#############NEWS#############

@server.post("/create-post", tags=["News"])
async def create_news(title: str = Form(...), description: str = Form(...), foto: UploadFile = File(...), genre: str = Form('other')):
    url = server.url_path_for('home')
    with open(f'{UPLOADED_IMG_PATH}/{foto.filename}', "wb") as uploaded_file:
        file_content = await foto.read()
        uploaded_file.write(file_content)
        uploaded_file.close()
        create_post(title, description, foto.filename, genre)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


#unused by A
@server.get("/news", tags=["News"])
def get_news():
    news = get_posts()
    if news:
        return news
    else:
        "News not exists"


@server.get("/news/", tags=["News"])
def get_news_by_id(id: int):
    post = get_post_by_id(id)
    if post:
        return post
    else:
        return "News not found"


@server.post("/update-news/", tags=["News"])
async def update_news(id: int = Form(...),
                title: str = Form(""),
                description: str = Form(""),
                foto: UploadFile = File(""),
                genre: str = Form('other')):
    url = server.url_path_for('home')
    with open(f'{UPLOADED_IMG_PATH}/{foto.filename}', "wb") as uploaded_file:
        file_content = await foto.read()
        uploaded_file.write(file_content)
        uploaded_file.close()
        update_post(id, title, description, foto.filename, genre)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/news/about/{id}')
async def about(request: Request, id: int):
    post = get_post_by_id(id)
    return templates.TemplateResponse("about.html", {"request": request, "post": post})

@server.get("/delete-post/", tags=["News"])
def delete_news(title: str):
    post = get_post(title)
    delete_post(post)
    return RedirectResponse("/")


@server.get("/news-search", tags=["News"])
def search_news(request: Request, text: str):
    s = search_post(text)
    return templates.TemplateResponse("index.html", {"request": request, "news": s})


#############FILES#############
"""@server.post("/lib/upload", tags=["Library"], status_code=status.HTTP_200_OK)
async def upload_file_to_lib(request: Request, file_upload: UploadFile = File(...)):
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" message=\"Starting upload {file_upload.filename}\" status=\"HTTP_200_OK\"')
    data = await file_upload.read()
    try:
        os.mkdir(UPLOADED_FILES_PATH)
    except FileExistsError:
        pass
    save_to = UPLOADED_FILES_PATH / file_upload.filename
    try:
        with open(save_to, 'ab') as f:
            f.write(data)
    except RuntimeError:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" message=\"Can\'t save file\"')

    logger.info(f'client=\"{request.client.host}:{request.client.port}\" message=\"File {file_upload.filename} successful uploaded\" status=\"HTTP_200_OK\"')
    return {"filename": file_upload.filename}"""


@server.post("/lib/upload", tags=["Library"], status_code=status.HTTP_200_OK)
async def upload_file(
        response: Response,
        annotation: str = Form("описания нет"),
        tag: str = Form("Статья"),
        upload_file: UploadFile = File(...),
):
    #user = get_current_user(token)
    # Format new filename
    url = server.url_path_for('library')
    file = upload_file
    full_name = format_filename(file, annotation, file.filename)

    # Save file
    # Шлях опять прикурил
    await save_file_to_uploads(file, tag)

    # Get file size
    file_size = human_read_format(get_file_size(file.filename, tag))

    # Get info from DB
    file_info_from_db = get_file_from_db(file.filename)

    # Add to DB
    if not file_info_from_db:
        response.status_code = status.HTTP_201_CREATED
        add_file_to_db(
            annotation=annotation,
            full_name=full_name,
            tag=tag,
            file_size=file_size,
            file=file,
        )
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get("/lib/download", tags=["Library"], status_code=status.HTTP_200_OK)
async def download_file(
        response: Response,
        name: str,
):
    file_info_from_db = get_file_from_db(name)

    if file_info_from_db:
        file_resp = FileResponse(UPLOADED_FILES_PATH + file_info_from_db.name,
                                 media_type=file_info_from_db.mime_type,
                                 filename=file_info_from_db.name)
        response.status_code = status.HTTP_200_OK
        return file_resp
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'msg': 'File not found'}


@server.delete("/lib/delete", tags=["Library"])
async def delete_file(
        response: Response,
        name: str,

):
    file_info_from_db = get_file_from_db(name)

    if file_info_from_db:
        # Delete file from DB
        delete_file_from_db(file_info_from_db)

        # Delete file from uploads
        delete_file_from_uploads(file_info_from_db.name, file_info_from_db.tag)

        response.status_code = status.HTTP_200_OK
        return {'msg': f'File {file_info_from_db.name} successfully deleted'}
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'msg': f'File does not exist'}


@server.get("/prepods", tags=["Prepods"])
def get_employees(request: Request):
    prepods = get_prepods()
    #return prepods
    return templates.TemplateResponse("prepods.html", {"request": request, "prepods": prepods})


@server.post("/create-prepod", tags=["Prepods"])
async def create_prepods(title: str = Form(...),
                description: str = Form(...),
                foto: UploadFile = File(...),):
    url = server.url_path_for('home')
    with open(f'{UPLOADED_IMG_PATH_PREPODS}/{foto.filename}', "wb") as uploaded_file:
        file_content = await foto.read()
        uploaded_file.write(file_content)
        uploaded_file.close()
        prep = create_prepod(title, description, foto.filename)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.post("/delete-prepod", tags=["Prepods"])
def delete_prepods(request: Request, fio: str = Form(...)):
    url = server.url_path_for('home')
    prepod = get_prepod(fio)
    try:
        delete_prepod(prepod)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" message=\"{fio} is deleted\"')
    except Exception as e:
        logger.error(e)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.post("/update-prepod/id", tags=["Prepods"])
async def update_prepods(id: int = Form(...),
                title: str = Form(...),
                description: str = Form(...),
                foto: UploadFile = File(...),):
    with open(f'{UPLOADED_IMG_PATH_PREPODS}/{foto.filename}', "wb") as uploaded_file:
        file_content = await foto.read()
        uploaded_file.write(file_content)
        uploaded_file.close()
    return update_prepod(id, title, description, foto.filename)


@server.get("/prepod/", tags=["Prepods"])
def get_prepods_by_id(id: int):
    post = get_prepod_by_id(id)
    if post:
        return post
    else:
        return "News not found"


@server.get('/casino', include_in_schema=False)
def foo():
    return {'success': True}


@server.get('/lib')
def library(request: Request):
    docs = get_file_from_db()
    return templates.TemplateResponse("lib.html", {"request": request, "docs": docs})


@server.get("/lib/show/{name}", tags=["Library"])
def watch_online(name):
    file = get_file_from_db(name)
    if file.mime_type == "application/pdf":
        pdf = get_content_file(file.name, file.tag)
        response = Response(pdf, media_type="application/pdf")
        return response
    else:
        return {"detail": "you can see just pdf's files"}


@server.get("/lib/show/", tags=["Library"])
def show_all_files():
    return get_file_from_db()


@server.get("/lib/search/subtr-{substr}", tags=["Library"])
def search_substr(substr: str):
    names = []
    files = get_file_from_db()
    #return get_content_file(files[0].name)
    for file in files:
        f = None
        if file.mime_type == "application/pdf":
            f = search_text_in_pdf(file, substr)
        elif file.mime_type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            f = search_text_in_docx(file, substr)
        if f is not None:
            names.append(file.name)
    return names


@server.get("/lib/search/tag-{substr}", tags=["Library"])
def search_by_tags(substr: str):
    names = []
    files = get_file_from_db()
    for file in files:
        if file.tag == substr:
            names.append(file.name)
    return names

@server.get("/lib/search/name-{substr}", tags=["Library"])
def search_by_name(substr: str):
    names = []
    files = get_file_from_db()
    for file in files:
        name = str(Path(file.name).with_suffix(''))
        if substr in name:
            names.append(file.name)
    return names



# if __name__ == "__main__":
#     uvicorn.run(server, host="0.0.0.0", port=8001)
