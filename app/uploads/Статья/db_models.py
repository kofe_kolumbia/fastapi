import enum
from datetime import datetime

from sqlalchemy import Column, Integer, String, Enum, ForeignKey, DateTime, null
from app.db_connect import Base
from sqlalchemy.orm import DeclarativeBase


class Roles(enum.Enum):
    user = 1
    employee = 2
    admin = 3


class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True, index=True)
    annotation = Column(String, nullable=True, default=null())
    name = Column(String)
    tag = Column(String)
    size = Column(String)
    mime_type = Column(String)
    modification_time = Column(String)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    role = Column(Enum(Roles))
    hashed_password = Column(String)
    avatar = Column(String, nullable=True)
    git_id = Column(Integer, nullable=True, default=null())
    git_username = Column(String, nullable=True, default=null())
    git_name = Column(String, nullable=True, default=null())


class News(Base):
    __tablename__ = "news"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=False, index=True)
    description = Column(String, unique=True, index=True)
    created_date = Column(DateTime, default=datetime.now().date())
    url_photo = Column(String, default="/app/uploads/news_photo/")
    genre = Column(String, default="other")



class Prepods(Base):
    __tablename__ = "prepods"

    id = Column(Integer, primary_key=True, index=True)
    avatar = Column(String, unique=True, nullable=True)
    title = Column(String, unique=True, nullable=True, index=True)
    description = Column(String, unique=True, nullable=True, index=True)
