function scrollToElement() {
    const element = document.getElementById('news-scroll');
    element.scrollIntoView({ behavior: 'smooth' });
}

function openModal() {
    const element = document.getElementById('loginModal');
    element.classList.add('show');
}

function closeModal() {
    const element = document.getElementById('loginModal');
    element.classList.remove('show');
}

function openModalImg() {
    const element = document.getElementById('loginModalImg');
    element.classList.add('show');
}

function closeModalImg() {
    const element = document.getElementById('loginModalImg');
    element.classList.remove('show');
}

window.onclick = function(event) {
    const element = document.getElementById('loginModalImg')
    if (event.target == element) {
    element.classList.remove('show');
    }
}

window.onclick = function(event) {
    const element = document.getElementById('loginModal')
    if (event.target == element) {
    element.classList.remove('show');
    }
}

function unlogin() {
    localStorage.removeItem('token');
    window.location.href = '/';
}

function git() {
    window.location.href = '/auth';
}

function toggleDropdown() {
    const dropdownContent = document.querySelector('.dropdown-content');
    if (dropdownContent.classList.contains('open')) {
        dropdownContent.classList.remove('open');
    } else {
        dropdownContent.classList.add('open');
    }
}

document.addEventListener('click', function(event) {
    const dropdown = document.querySelector('.dropdown');
        if (!dropdown.contains(event.target)) {
            const dropdownContent = document.querySelector('.dropdown-content');
            dropdownContent.classList.remove('open');
        }
});