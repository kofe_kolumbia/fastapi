document.addEventListener('DOMContentLoaded', function() {
      const table = document.getElementById('myTable');
      const showPreviousButton = document.getElementById('showPrevious');
      const showNextButton = document.getElementById('showNext');
      const pageCounter = document.getElementById('pageCounter');
      const rows = table.rows;
      const rowLimit = 10;
      let currentPage = 1;

      function showPage(pageNumber) {
        for (let i = 1; i < rows.length; i++) {
          if (i >= (pageNumber - 1) * rowLimit + 1 && i <= pageNumber * rowLimit) {
            rows[i].style.display = 'table-row';
          } else {
            rows[i].style.display = 'none';
          }
        }
        let pagesTotal = Math.ceil((rows.length - 1) / rowLimit);
        if (pagesTotal === 0) {
            pagesTotal = 1;
        }
        pageCounter.textContent = `Страница ${pageNumber} / ${pagesTotal}`;
      }

      showPreviousButton.addEventListener('click', function() {
        if (currentPage > 1) {
          currentPage--;
          showPage(currentPage);
        }
      });

      showNextButton.addEventListener('click', function() {
        if (currentPage < Math.ceil(rows.length / rowLimit)) {
          currentPage++;
          showPage(currentPage);
        }
      });

      showPage(currentPage);
    });