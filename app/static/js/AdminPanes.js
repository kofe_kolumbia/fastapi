function toggleVisibility(visibleElementId) {
    const ids = ['addNews', 'deleteNews', 'changeNews'];

    ids.forEach(id => {
        const element = document.getElementById(id);
        if (id === visibleElementId) {
            element.classList.add('visible');
        } else {
            element.classList.remove('visible');
        }
    });
}

function toggleVisibilityTeacher(visibleElementId) {
    const ids = ['addTeacher', 'deleteTeacher', 'changeTeacher'];

    ids.forEach(id => {
        const element = document.getElementById(id);
        if (id === visibleElementId) {
            element.classList.add('visible');
        } else {
            element.classList.remove('visible');
        }
    });
}

function toggleVisibilityDocument(visibleElementId) {
    const ids = ['addDoc', 'deleteDoc'];

    ids.forEach(id => {
        const element = document.getElementById(id);
        if (id === visibleElementId) {
            element.classList.add('visible');
        } else {
            element.classList.remove('visible');
        }
    });
}

function toggleVisibilityRoles(visibleElementId) {
    const ids = ['changeRoles'];

    ids.forEach(id => {
        const element = document.getElementById(id);
        if (id === visibleElementId) {
            element.classList.add('visible');
        } else {
            element.classList.remove('visible');
        }
    });
}